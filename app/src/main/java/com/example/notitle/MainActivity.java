package com.example.notitle;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView tvClimaChillan, tvHumedadChillan, tvPresionChillan;
    private TextView tvClimaSantiago, tvHumedadSantiago, tvPresionSantiago;
    private TextView tvClimaConcepcion, tvHumedadConcepcion, tvPresionConcepcion;
    private Button button, button2, button3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvClimaChillan = (TextView) findViewById(R.id.tvClimaChillan);
        this.tvPresionChillan = (TextView) findViewById(R.id.tvPresionChillan);
        this.tvHumedadChillan = (TextView) findViewById(R.id.tvHumedadChillan);
        this.tvClimaSantiago = (TextView) findViewById(R.id.tvClimaSantiago);
        this.tvPresionSantiago = (TextView) findViewById(R.id.tvPresionSantiago);
        this.tvHumedadSantiago = (TextView) findViewById(R.id.tvHumedadSantiago);
        this.tvClimaConcepcion = (TextView) findViewById(R.id.tvClimaConcepcion);
        this.tvPresionConcepcion = (TextView) findViewById(R.id.tvPresionConcepcion);
        this.tvHumedadConcepcion = (TextView) findViewById(R.id.tvHumedadConcepcion);
        this.button = (Button) findViewById(R.id.button);

        this.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6066399&lon=-72.1034393&appid=5418fca3533e20d541db613bf804a872&units=metric"; //chillan
                StringRequest solicitud1 = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);
                                    JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                                    int temp = mainJSON.getInt("temp");
                                    int humedad = mainJSON.getInt("humidity");
                                    int presion = mainJSON.getInt("pressure");
                                    tvClimaChillan.setText("Temperatura = " + temp + "°c");
                                    tvHumedadChillan.setText("Humedad = " + humedad);
                                    tvPresionChillan.setText("Precion = " + presion);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Algo fallo
                            }
                        }
                );


                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud1);

                String url2 = "http://api.openweathermap.org/data/2.5/weather?lat=-33.4718999&lon=-70.9100218&appid=5418fca3533e20d541db613bf804a872&units=metric"; //santiago
                StringRequest solicitud2 = new StringRequest(
                        Request.Method.GET,
                        url2,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);
                                    JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                                    int temp = mainJSON.getInt("temp");
                                    int humedad = mainJSON.getInt("humidity");
                                    int presion = mainJSON.getInt("pressure");
                                    tvClimaSantiago.setText("Temperatura = " + temp + "°c");
                                    tvHumedadSantiago.setText("Humedad = " + humedad);
                                    tvPresionSantiago.setText("Precion = " + presion);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Algo fallo
                            }
                        }
                );


                RequestQueue listaEspera2 = Volley.newRequestQueue(getApplicationContext());
                listaEspera2.add(solicitud2);

                String url3 = "http://api.openweathermap.org/data/2.5/weather?lat=-36.8282&lon=-73.0514&appid=5418fca3533e20d541db613bf804a872&units=metric"; //conce
                StringRequest solicitud3 = new StringRequest(
                        Request.Method.GET,
                        url3,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);
                                    JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                                    int temp = mainJSON.getInt("temp");
                                    int humedad = mainJSON.getInt("humidity");
                                    int presion = mainJSON.getInt("pressure");
                                    tvClimaConcepcion.setText("Temperatura = " + temp + "°c");
                                    tvHumedadConcepcion.setText("Humedad = " + humedad);
                                    tvPresionConcepcion.setText("Precion = " + presion);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Algo fallo
                            }
                        }
                );


                RequestQueue listaEspera3 = Volley.newRequestQueue(getApplicationContext());
                listaEspera3.add(solicitud3);

            }


        });
    }
}

